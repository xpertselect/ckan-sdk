<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk;

use XpertSelect\PsrTools\PsrResponse;

/**
 * Class CkanResponse.
 */
final class CkanResponse extends PsrResponse
{
    /**
     * {@inheritdoc}
     */
    public function getJsonSchemaPath(): string
    {
        return __DIR__ . '/../resources/json-schemas/';
    }
}
