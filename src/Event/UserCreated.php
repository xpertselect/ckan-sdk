<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

/**
 * Class UserCreated.
 *
 * A user create event that should be fired on user CREATE actions.
 */
final class UserCreated extends UserSaved
{
}
