<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

/**
 * Class DatasetCreated.
 *
 * A dataset create event that should be fired on dataset CREATE actions.
 */
final class DatasetCreated extends DatasetSaved
{
}
