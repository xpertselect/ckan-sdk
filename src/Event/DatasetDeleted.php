<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class DatasetDeleted.
 *
 * A dataset delete event that should be fired on dataset DELETE actions.
 */
final class DatasetDeleted extends StoppableEvent
{
    use IdentifiesInstance;

    /**
     * DatasetDeleted constructor.
     *
     * @param null|string $instanceId The (optional) ID of the CKAN instance
     * @param string      $nameOrId   The CKAN ID or name of the deleted dataset
     * @param null|string $identifier The global identifier of the deleted dataset
     */
    public function __construct(?string $instanceId, private readonly string $nameOrId,
                                private readonly ?string $identifier = null)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Get the CKAN ID or name of the deleted dataset.
     *
     * @return string The CKAN ID or name of the deleted dataset
     */
    public function getNameOrId(): string
    {
        return $this->nameOrId;
    }

    /**
     * Gets the global identifier of the dataset that is deleted.
     *
     * @return null|string The global identifier of the dataset that is deleted
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }
}
