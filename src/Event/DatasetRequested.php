<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class DatasetRequested.
 *
 * Event indicating that a dataset is being requested from the CKAN API.
 */
class DatasetRequested extends StoppableEvent
{
    use IdentifiesInstance;

    /**
     * The resolved dataset object.
     *
     * @var null|array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * }
     */
    private ?array $resolvedDataset = null;

    /**
     * DatasetRequested constructor.
     *
     * @param null|string $instanceId       The (optional) ID of the CKAN instance
     * @param string      $requestedDataset The ID or name of the requested dataset
     */
    public function __construct(?string $instanceId, private readonly string $requestedDataset)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Get the ID or name of the requested dataset.
     *
     * @return string The ID or name of the dataset
     */
    public function getRequestedDataset(): string
    {
        return $this->requestedDataset;
    }

    /**
     * Determine if the event holds a resolved dataset.
     *
     * @return bool Whether a resolved dataset is assigned
     */
    public function hasResolvedDataset(): bool
    {
        return !is_null($this->resolvedDataset);
    }

    /**
     * Return the resolved dataset.
     *
     * @return null|array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * }
     */
    public function getResolvedDataset(): ?array
    {
        return $this->resolvedDataset;
    }

    /**
     * Set the dataset.
     *
     * @param null|array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } $dataset The dataset object to assign
     */
    public function setResolvedDataset(?array $dataset): void
    {
        $this->resolvedDataset = $dataset;
    }
}
