<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

/**
 * Class DatasetUpdated.
 *
 * A dataset update event that should be fired on dataset UPDATE actions.
 */
final class DatasetUpdated extends DatasetSaved
{
}
