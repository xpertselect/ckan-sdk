<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class DatasetReceived.
 *
 * Event describing that a dataset has been received from the CKAN API.
 */
class DatasetReceived extends StoppableEvent
{
    use IdentifiesInstance;

    /**
     * DatasetReceived constructor.
     *
     * @param null|string $instanceId The (optional) ID of the CKAN instance
     * @param array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } $dataset The dataset received from CKAN
     */
    public function __construct(?string $instanceId,
                                public readonly array $dataset)
    {
        $this->instanceId = $instanceId;
    }
}
