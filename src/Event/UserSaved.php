<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class UserSaved.
 *
 * An abstract user save event that is extended for user CREATE events.
 */
abstract class UserSaved extends StoppableEvent
{
    use IdentifiesInstance;

    /**
     * UserSaved constructor.
     *
     * @param null|string $instanceId The ID of the CKAN instance
     * @param string      $userId     The CKAN ID of the saved user
     */
    public function __construct(?string $instanceId, private readonly string $userId)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Get the CKAN ID of the saved user.
     *
     * @return string The CKAN ID of the saved user
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}
