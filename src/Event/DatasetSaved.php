<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Event;

use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\StoppableEvent;

/**
 * Class DatasetSaved.
 *
 * An abstract dataset save event that is extended for dataset CREATE, UPDATE and PATCH events.
 */
abstract class DatasetSaved extends StoppableEvent
{
    use IdentifiesInstance;

    /**
     * DatasetSaved constructor.
     *
     * @param null|string $instanceId The (optional) ID of the CKAN instance
     * @param string      $nameOrId   The CKAN ID or name of the saved dataset
     */
    public function __construct(?string $instanceId, private readonly string $nameOrId)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Get the CKAN ID or name of the saved dataset.
     *
     * @return string The CKAN ID or name of the saved dataset
     */
    public function getNameOrId(): string
    {
        return $this->nameOrId;
    }
}
