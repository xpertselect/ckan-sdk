<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class OrganizationRepository.
 *
 * Provides access to the organization API routes offered by CKAN.
 */
class OrganizationRepository
{
    use IdentifiesInstance;

    /**
     * OrganizationRepository constructor.
     *
     * @param HttpRequestService $httpService The service for interacting with the HTTP API
     * @param null|string        $instanceId  The (optional) ID of the CKAN instance
     */
    public function __construct(protected HttpRequestService $httpService,
                                ?string $instanceId = null)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Request properties of a user by performing a `api/3/action/organization_show` API call and
     * returning its response.
     *
     * @param string $nameOrId The name or id of the organization
     *
     * @return array{
     *     users: array<string, mixed>,
     *     display_name: string,
     *     description: string,
     *     image_display_url: string,
     *     package_count: int,
     *     created: string,
     *     name: string,
     *     is_organization: bool,
     *     state: string,
     *     extras: array<mixed, mixed>,
     *     image_url: string,
     *     groups: array<mixed, mixed>,
     *     type: string,
     *     title: string,
     *     revision_id: string,
     *     num_followers: int,
     *     id: string,
     *     tags: array<mixed, mixed>,
     *     approval_status: string
     * } The properties of an organization
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function get(string $nameOrId): array
    {
        $response = $this->httpService->get('api/3/action/organization_show', ['id' => $nameOrId]);

        if ($response->hasStatus(200) && $response->hasValidJson('organization_show.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Request the list of organizations and its properties by performing a
     * `api/3/action/organization_list` API call and returning its response.
     *
     * @return array<int, array<string, mixed>> The list of organizations and its properties
     *
     * @throws ResponseException Thrown when an API request did not succeed for any reason
     * @throws ClientException   Thrown when a request could not be sent
     */
    public function list(): array
    {
        $organizations = [];
        $offset        = 0;

        // Assume that the limit is set to 25 in CKAN config
        $limit = 25;

        do {
            $organizationPage = $this->httpService
                ->get('api/3/action/organization_list', [
                    'all_fields' => true,
                    'offset'     => $offset,
                ]);

            if (!$organizationPage->hasStatus(200) || !$organizationPage->hasValidJson('organization_list.json')) {
                throw new ResponseException($organizationPage);
            }

            $organizationPage = $organizationPage->json(true)['result'];
            $offset += $limit;
            $gotAllOrganizations = count($organizationPage) < $limit;
            $organizations       = array_merge($organizations, $organizationPage);
        } while (!$gotAllOrganizations);

        return $organizations;
    }

    /**
     * Request the list of organizations and its properties for a user by performing a
     * '/api/3/action/organization_list_for_user' API call and returning its response.
     * Only organizations for which the given user has the given permission will be returned.
     *
     * @param string $nameOrId   The CKAN user ID to generate the organization list for
     * @param string $permission The permission that the user should have on all retrieved
     *                           organizations
     *
     * @return array<int, array<string, mixed>> The CKAN API response
     *
     * @throws ResponseException Thrown when an API request did not succeed for any reason
     * @throws ClientException   Thrown when a request could not be sent
     */
    public function listForUser(string $nameOrId, string $permission = 'read'): array
    {
        $response = $this->httpService->get('api/3/action/organization_list_for_user', [
            'all_fields' => 'true',
            'limit'      => 1000,
            'id'         => $nameOrId,
            'permission' => $permission,
        ]);

        if ($response->hasStatus(200) && $response->hasValidJson('organization_list_for_user.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Retrieve the `{organization id} => {organization display name}` pairs by listing all
     * organizations.
     *
     * @param string $labelField The field to use as the organization name
     *
     * @return array<string, mixed> The list of organizations as id-name pairs
     *
     * @throws ClientException   Thrown when the API request failed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     *
     * @see OrganizationRepository::list()
     */
    public function names(string $labelField = 'display_name'): array
    {
        return $this->flattenOrganizations($this->list(), $labelField);
    }

    /**
     * Retrieve the `{organization id} => {organization display name}` pairs by listing all
     * organizations for the given user. Only organizations for which the given user has the given
     * permission will be returned.
     *
     * @param string $userId     The CKAN user ID to generate the organization list for
     * @param string $permission The permission that the user should have on all retrieved
     *                           organizations
     * @param string $labelField The field to use as the {organization name}. Defaults to
     *                           'display_name'
     *
     * @return array<string, mixed> The list of organizations for the given user as id-name pairs
     *
     * @throws ClientException   Thrown when the API request failed for any reason
     * @throws ResponseException Thrown when the request could not be sent
     */
    public function namesForUser(string $userId, string $permission = 'read',
                                 string $labelField = 'display_name'): array
    {
        return $this->flattenOrganizations(
            $this->listForUser($userId, $permission), $labelField
        );
    }

    /**
     * Flattens an array of CKAN organization dictionaries into an
     * `{organization id} => {organization name}` array.
     *
     * @param array<int, array<string, mixed>> $organizations The organization dictionaries to
     *                                                        flatten
     * @param string                           $labelField    the field to use as the
     *                                                        {organization name}
     *
     * @return array<string, mixed> The flattened CKAN organizations
     */
    private function flattenOrganizations(array $organizations,
                                          string $labelField = 'display_name'): array
    {
        $names = [];

        foreach ($organizations as $organization) {
            if (!array_key_exists('id', $organization)) {
                continue;
            }

            if (!array_key_exists($labelField, $organization)) {
                continue;
            }

            $names[strval($organization['id'])] = $organization[$labelField];
        }

        return $names;
    }
}
