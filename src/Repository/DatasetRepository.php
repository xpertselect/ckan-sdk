<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use Psr\EventDispatcher\EventDispatcherInterface;
use RuntimeException;
use XpertSelect\CkanSdk\Event\DatasetCreated;
use XpertSelect\CkanSdk\Event\DatasetDeleted;
use XpertSelect\CkanSdk\Event\DatasetPatched;
use XpertSelect\CkanSdk\Event\DatasetReceived;
use XpertSelect\CkanSdk\Event\DatasetRequested;
use XpertSelect\CkanSdk\Event\DatasetUpdated;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\DispatchesEvents;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * Class DatasetRepository.
 *
 * Provides access to the dataset/package API routes offered by CKAN.
 */
class DatasetRepository
{
    use DispatchesEvents;
    use IdentifiesInstance;

    /**
     * DatasetRepository constructor.
     *
     * @param HttpRequestService            $httpService     The service for interacting with the
     *                                                       HTTP API
     * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
     * @param null|string                   $instanceId      The (optional) ID of the CKAN instance
     */
    public function __construct(protected readonly HttpRequestService $httpService,
                                ?EventDispatcherInterface $eventDispatcher = null,
                                ?string $instanceId = null)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->instanceId      = $instanceId;
    }

    /**
     * Counts the number of datasets by performing a `api/3/action/package_search` API call and
     * returning the count part of its response.
     *
     * @param array<string, mixed> $query The set of key-value parameters to send as query
     *                                    parameters to the search endpoint
     *
     * @return int The number of datasets
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function count(array $query = ['q' => '*:*', 'include_private' => true]): int
    {
        $query['rows'] = 0;

        return $this->search($query)['count'];
    }

    /**
     * Get all datasets by performing `api/3/action/package_search` API calls until all datasets
     * are fetched.
     *
     * @param array<string, mixed> $query The set of key-value parameters to send as query
     *                                    parameters to the search endpoint
     *
     * @return array<int, array<string, mixed>> All datasets
     *
     * @throws ResponseException Thrown when one of the API requests did not succeed for any reason
     * @throws ClientException   Thrown when one of the requests could not be sent
     */
    public function all(array $query = ['q' => '*:*', 'include_private' => true]): array
    {
        $fetched  = 0;
        $start    = 0;
        $rows     = 10;
        $datasets = [];

        do {
            $query['start'] = $start;
            $query['rows']  = $rows;
            $query['sort']  = 'metadata_created asc';
            $result         = $this->search($query);

            $datasets = array_merge($datasets, $result['results']);
            $fetched += count($result['results']);
            $start   += $rows;
        } while ($fetched < $result['count']);

        return $datasets;
    }

    /**
     * Request properties of a dataset by performing a `api/3/action/package_show` API call and
     * returning its response.
     *
     * Dispatches a `DatasetRequested` event prior to communicating with the API. If the result of
     * the dispatched event resolved to the requested dataset, then the dataset from the dispatched
     * event will be returned and no communication with the CKAN API will take place. A usage
     * example may be to load the requested dataset from a caching layer in order to reduce the
     * communication with the CKAN API.
     *
     * Dispatches a `DatasetReceived` event after receiving the dataset from the CKAN API. The event
     * includes the received dataset. A usage example may be to store the received dataset in a
     * caching layer. Any output resulting from dispatching this event is ignored.
     *
     * @param string $nameOrId The name or id of the dataset
     *
     * @return array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The properties of the dataset
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     *
     * @see DatasetRequested
     * @see DatasetReceived
     */
    public function get(string $nameOrId): array
    {
        $datasetRequested = $this->dispatchEvent(new DatasetRequested($this->instanceId, $nameOrId));

        if ($dataset = $this->eventResolvesRequestedDataset($datasetRequested)) {
            return $dataset;
        }

        $response = $this->httpService->get('api/3/action/package_show', ['id' => $nameOrId]);

        if ($response->hasStatus(200) && $response->hasValidJson('package_show.json')) {
            $dataset = $response->json(true)['result'];

            $this->dispatchEvent(new DatasetReceived($this->instanceId, $dataset));

            return $dataset;
        }

        throw new ResponseException($response);
    }

    /**
     * Perform a search on datasets by performing a `api/3/action/package_search` API call and
     * returning its response.
     *
     * @param array<string, mixed> $query The set of key-value parameters to send as query
     *                                    parameters to the search endpoint
     *
     * @return array{
     *     count: int,
     *     sort: string,
     *     results: array<int, array<string, mixed>>,
     *     facets: null|array<string, array<string, int>>
     * } The search result
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function search(array $query): array
    {
        $response = $this->httpService->get('api/3/action/package_search', $query);

        if ($response->hasStatus(200) && $response->hasValidJson('package_search.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Request the list of public dataset names by performing a `api/3/action/package_list` API call
     * and returning its response.
     *
     * @return string[] The list of public dataset names
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function list(): array
    {
        $response = $this->httpService->get('api/3/action/package_list');

        if ($response->hasStatus(200) && $response->hasValidJson('package_list.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Creates a dataset with the given properties by performing a `api/3/action/package_create` API
     * call and returning its response.
     *
     * @param array<string, mixed> $data The properties of the dataset to be created
     *
     * @return array{
     *     creator_user_id: string,
     *     id: string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The properties of the created dataset
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function create(array $data): array
    {
        $response = $this->httpService->post('api/3/action/package_create', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('package_create.json')) {
            $result = $response->json(true)['result'];

            $this->dispatchEvent(new DatasetCreated($this->instanceId, $result['id']));

            return $result;
        }

        throw new ResponseException($response);
    }

    /**
     * Updates a dataset with the given properties by performing a `api/3/action/package_update` API
     * call and returning its response.
     *
     * @param string               $nameOrId The name or id of the dataset to be updated
     * @param array<string, mixed> $data     The properties of the dataset to be updated
     *
     * @return array{
     *     creator_user_id: string,
     *     id: string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The properties of the updated dataset
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function update(string $nameOrId, array $data): array
    {
        $data['id'] = $nameOrId;
        $response   = $this->httpService->post('api/3/action/package_update', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('package_update.json')) {
            $result = $response->json(true)['result'];

            $this->dispatchEvent(new DatasetUpdated($this->instanceId, $result['id']));

            return $result;
        }

        throw new ResponseException($response);
    }

    /**
     * Create or update a dataset with the given properties.
     *
     * @param array<string, mixed> $data The properties of the dataset to create or update
     *
     * @return array{
     *     creator_user_id: string,
     *     id: string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The properties of the created or updated dataset
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     *
     * @see DatasetRepository::exists()
     * @see DatasetRepository::create()
     * @see DatasetRepository::update())
     */
    public function upsert(array $data): array
    {
        return $this->exists($data['name'])
            ? $this->update($data['name'], $data)
            : $this->create($data);
    }

    /**
     * Patches a dataset with the given properties by performing a `api/3/action/package_patch` API
     * call and returning its response.
     *
     * @param string               $nameOrId The name or id of the dataset to be patched
     * @param array<string, mixed> $data     The properties of the dataset to be patched
     *
     * @return array{
     *     creator_user_id: string,
     *     id: string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The properties of the patched dataset
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function patch(string $nameOrId, array $data): array
    {
        $data['id'] = $nameOrId;
        $response   = $this->httpService->post('api/3/action/package_patch', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('package_patch.json')) {
            $result = $response->json(true)['result'];

            $this->dispatchEvent(new DatasetPatched($this->instanceId, $result['id']));

            return $result;
        }

        throw new ResponseException($response);
    }

    /**
     * Deletes or purges a dataset with the given name or id by fetching the dataset first and then
     * performing a `api/3/action/package_delete` or `api/3/action/dataset_purge` API call and
     * returning its response.
     *
     * @param string $nameOrId   The name or id of the dataset to be deleted or purged
     * @param bool   $softDelete Whether to soft-delete or delete the dataset permanently
     *
     * @return bool Whether the deletion was a success
     *
     * @throws ResponseException Thrown when one of the API requests did not succeed for any reason
     * @throws ClientException   Thrown when one of the requests could not be sent
     */
    public function delete(string $nameOrId, bool $softDelete = false): bool
    {
        $dataset  = $this->get($nameOrId);
        $endpoint = $softDelete ? 'api/3/action/package_delete' : 'api/3/action/dataset_purge';
        $response = $this->httpService->post($endpoint, new Payload(['id' => $nameOrId]));

        if ($response->hasStatus(200) && $response->hasValidJson('package_delete-dataset_purge.json')) {
            $success = true === $response->json(true)['success'];

            if ($success) {
                $this->dispatchEvent(new DatasetDeleted(
                    $this->instanceId,
                    $nameOrId,
                    $dataset['identifier'] ?? null
                ));
            }

            return $success;
        }

        throw new ResponseException($response);
    }

    /**
     * Checks whether a given dataset exists. If the dataset can be fetched successfully return
     * true, else if the HTTP status code of the response is 403 or 404 return false.
     *
     * @param string $nameOrId The CKAN ID or name of the dataset
     *
     * @return bool Whether the dataset exists in CKAN
     *
     * @throws ResponseException Thrown when the API request failed for any reason except for status
     *                           codes 403 and 404
     * @throws ClientException   Thrown when the request could not be sent
     *
     * @see DatasetRepository::get()
     */
    public function exists(string $nameOrId): bool
    {
        try {
            $this->get($nameOrId);

            return true;
        } catch (ResponseException $e) {
            if ($e->response->hasStatus(403) || $e->response->hasStatus(404)) {
                return false;
            }

            throw $e;
        }
    }

    /**
     * Generates a CKAN compliant name for a given title. The generated name will adhere to the
     * following.
     *
     * - It is unique, no other package in CKAN has an identical name. Uniqueness is assured by
     *   appending an incrementing integer to the name until a version is found that isn't used yet.
     *   Uses the `exists()` method to determine if a name is already in use.
     * - It contains only the following characters: [a-z0-9-]. Uppercase characters will be
     *   converted to lowercase. any non-alphanumeric character will be replaced with a '-'.
     * - The length of the name will not exceed 100 characters, which is a limit that CKAN enforces.
     *
     * @param string $title  The title to base the generated name on
     * @param string $prefix An optional prefix to prepend to the generated name
     *
     * @return string The generated CKAN name
     *
     * @throws ResponseException Thrown when the API request needed for checking whether a name
     *                           already exists failed
     * @throws ClientException   Thrown when the request needed for checking whether a name already
     *                           exists could not be sent
     *
     * @see DatasetRepository::exists()
     */
    public function generatePackageName(string $title, string $prefix = ''): string
    {
        $name = mb_strtolower($title);

        if (!empty($prefix)) {
            $name = mb_strtolower($prefix) . '-' . $name;
        }

        $name = preg_replace('/[^a-z0-9]+/', '-', $name);

        if (is_null($name)) {
            throw new RuntimeException('An error occurred while performing regular expression search and replace');
        }

        if (strlen($name) >= 90) {
            $name = substr($name, 0, 90);
        }

        $baseName = $name;
        $number   = 1;

        while ($this->exists($name)) {
            $name = $baseName . '-' . $number;
            ++$number;
        }

        return $name;
    }

    /**
     * Determine if after dispatching the DatasetRequested event a handler has resolved the dataset.
     *
     * @param null|object $datasetRequested The result of the event dispatching
     *
     * @return null|array{
     *     creator_user_id: string,
     *     id: string,
     *     identifier: ?string,
     *     isopen: bool,
     *     license_id: string,
     *     license_title: string,
     *     license_url: string,
     *     metadata_created: string,
     *     metadata_modified: string,
     *     name: string,
     *     notes: string,
     *     num_resources: int,
     *     num_tags: int,
     *     organization: array<string, mixed>,
     *     owner_org: string,
     *     private: bool,
     *     state: string,
     *     title: string,
     *     type: string,
     *     groups: array<int, array<string, mixed>>,
     *     resources: array<int, array<string, mixed>>,
     *     tags: array<int, array<string, mixed>>
     * } The resolved dataset
     */
    private function eventResolvesRequestedDataset(?object $datasetRequested): ?array
    {
        if (!$datasetRequested instanceof DatasetRequested) {
            return null;
        }

        if (!$datasetRequested->hasResolvedDataset()) {
            return null;
        }

        return $datasetRequested->getResolvedDataset();
    }
}
