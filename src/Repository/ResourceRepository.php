<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\Event\DatasetUpdated;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\DispatchesEvents;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * Class ResourceRepository.
 *
 * Provides access to the resource API routes offered by CKAN.
 */
class ResourceRepository
{
    use DispatchesEvents;
    use IdentifiesInstance;

    /**
     * ResourceRepository constructor.
     *
     * @param HttpRequestService            $httpService     The service for sending HTTP requests
     * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
     * @param null|string                   $instanceId      The (optional) ID of the CKAN instance
     */
    public function __construct(protected readonly HttpRequestService $httpService,
                                ?EventDispatcherInterface $eventDispatcher = null,
                                ?string $instanceId = null
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->instanceId      = $instanceId;
    }

    /**
     * Request properties of a resource by performing a `api/3/action/resource_show` API call and
     * returning its response.
     *
     * @param string $nameOrId The name or id of the resource
     *
     * @return array{
     *     created: string,
     *     description: string,
     *     format: string,
     *     id: string,
     *     last_modified: string,
     *     mimetype: string,
     *     name: string,
     *     package_id: string,
     *     position: int,
     *     state: string,
     *     url: string
     * } The properties of a resource
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function get(string $nameOrId): array
    {
        $response = $this->httpService->get('api/3/action/resource_show', ['id' => $nameOrId]);

        if ($response->hasStatus(200) && $response->hasValidJson('resource_show.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Creates a resource with the given properties by performing a
     * `api/3/action/resource_create` API call and returning its response.
     *
     * Dispatches a `DatasetUpdated` event to signal that the parent dataset has been changed as a
     * result of this call.
     *
     * @param array<string, mixed> $data The properties of the resource to be created
     *
     * @return array{
     *     created: string,
     *     description: string,
     *     format: string,
     *     id: string,
     *     last_modified: string,
     *     mimetype: string,
     *     name: string,
     *     package_id: string,
     *     position: int,
     *     state: string,
     *     url: string
     * } The properties of the created resource
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function create(array $data): array
    {
        $response = $this->httpService->post('api/3/action/resource_create', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('resource_create.json')) {
            $this->dispatchEvent(new DatasetUpdated($this->instanceId, $data['package_id']));

            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Updates a resource with the given properties by performing a
     * `api/3/action/resource_update` API call and returning its response.
     *
     * Dispatches a `DatasetUpdated` event to signal that the parent dataset has been changed as a
     * result of this call.
     *
     * @param string               $nameOrId The name or id of the resource to be updated
     * @param array<string, mixed> $data     The properties of the resource to be updated
     *
     * @return array{
     *     created: string,
     *     description: string,
     *     format: string,
     *     id: string,
     *     last_modified: string,
     *     mimetype: string,
     *     name: string,
     *     package_id: string,
     *     position: int,
     *     state: string,
     *     url: string
     * } The properties of the updated resource
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function update(string $nameOrId, array $data): array
    {
        $data['id'] = $nameOrId;
        $response   = $this->httpService->post('api/3/action/resource_update', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('resource_update.json')) {
            $this->dispatchEvent(new DatasetUpdated($this->instanceId, $data['package_id']));

            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Patches a resource with the given properties by performing a
     * `api/3/action/resource_patch` API call and returning its response.
     *
     * Dispatches a `DatasetUpdated` event to signal that the parent dataset has been changed as a
     * result of this call.
     *
     * @param string               $nameOrId The name or id of the resource to be patched
     * @param array<string, mixed> $data     The properties of the resource to be patched
     *
     * @return array{
     *     created: string,
     *     description: string,
     *     format: string,
     *     id: string,
     *     last_modified: string,
     *     mimetype: string,
     *     name: string,
     *     package_id: string,
     *     position: int,
     *     state: string,
     *     url: string
     * } The properties of the patched resource
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function patch(string $nameOrId, array $data): array
    {
        $data['id'] = $nameOrId;
        $response   = $this->httpService->post('api/3/action/resource_patch', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('resource_patch.json')) {
            $this->dispatchEvent(new DatasetUpdated($this->instanceId, $data['package_id']));

            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Deletes a resource with the given name or id by performing a
     * `api/3/action/resource_delete` API call and returning its response.
     *
     * Dispatches a `DatasetUpdated` event to signal that the parent dataset has been changed as a
     * result of this call. In order for this event to be fired, this method will first retrieve the
     * resource from the API via `ResourceRepository::get` in order to gain access to the ID of the
     * parent dataset.
     *
     * @param string $nameOrId The name or id of the resource to be deleted
     *
     * @return bool Whether the deletion was a success
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     *
     * @see ResourceRepository::get()
     */
    public function delete(string $nameOrId): bool
    {
        $resource = $this->get($nameOrId);
        $response = $this->httpService->post('api/3/action/resource_delete', new Payload(['id' => $nameOrId]));

        if ($response->hasStatus(200) && $response->hasValidJson('resource_delete.json')) {
            $this->dispatchEvent(new DatasetUpdated($this->instanceId, $resource['package_id']));

            return true === $response->json(true)['success'];
        }

        throw new ResponseException($response);
    }

    /**
     * Load a given resource from a given dataset.
     *
     * @param string               $id      The CKAN ID of the resource
     * @param array<string, mixed> $dataset The dataset that may contain the resource
     *
     * @return null|array<string, mixed> The resource as it exists in the dataset, or null
     */
    public function getFromDataset(string $id, array $dataset): ?array
    {
        $resources = $dataset['resources'] ?? [];

        if (!is_array($resources)) {
            return null;
        }

        $resource = array_filter($resources, function($resource) use ($id) {
            return is_array($resource)
                && array_key_exists('id', $resource)
                && $resource['id'] === $id;
        });

        return array_pop($resource);
    }
}
