<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use Psr\EventDispatcher\EventDispatcherInterface;
use Random\Randomizer;
use XpertSelect\CkanSdk\Event\UserCreated;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\DispatchesEvents;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * Class UserRepository.
 *
 * Provides access to the user API routes offered by CKAN.
 */
class UserRepository
{
    use DispatchesEvents;
    use IdentifiesInstance;

    /**
     * UserRepository constructor.
     *
     * @param HttpRequestService            $httpService     The service for interacting with the
     *                                                       HTTP API
     * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
     * @param null|string                   $instanceId      The (optional) ID of the CKAN instance
     */
    public function __construct(protected HttpRequestService $httpService,
                                ?EventDispatcherInterface $eventDispatcher = null,
                                ?string $instanceId = null)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->instanceId      = $instanceId;
    }

    /**
     * Request properties of a user by performing a `api/3/action/user_show` API call and
     * returning its response.
     *
     * @param string $nameOrId The name or id of the user
     *
     * @return array{
     *     email_hash: string,
     *     about: ?string,
     *     display_name: string,
     *     name: string,
     *     created: string,
     *     id: string,
     *     sysadmin: bool,
     *     activity_streams_email_notifications: bool,
     *     state: string,
     *     number_of_edits: int,
     *     fullname: ?string,
     *     number_created_packages: int,
     *     apikey: ?string,
     *     email: ?string,
     *     num_followers: ?int,
     *     password_hash: ?string
     * } The properties of a user
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function get(string $nameOrId): array
    {
        $response = $this->httpService->get('api/3/action/user_show', ['id' => $nameOrId]);

        if ($response->hasStatus(200) && $response->hasValidJson('user_show.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Request the list of users and optionally its properties by performing a
     * `api/3/action/user_list` API call and returning its response.
     *
     * @return string[]|array<int, array<string, mixed>> The list of users and optionally its
     *                                                   properties
     *
     * @throws ResponseException Thrown when an API request did not succeed for any reason
     * @throws ClientException   Thrown when a request could not be sent
     */
    public function list(bool $allFields = true): array
    {
        $response = $this->httpService->get('api/3/action/user_list', [
            'all_fields' => $allFields,
        ]);

        if ($response->hasStatus(200) && $response->hasValidJson('user_list.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Creates a user with the given properties by performing a
     * `api/3/action/user_create` API call and returning its response.
     *
     * @param array<string, mixed> $data The properties of the user to be created
     *
     * @return array{
     *     email_hash: string,
     *     about: ?string,
     *     display_name: string,
     *     name: string,
     *     created: string,
     *     id: string,
     *     sysadmin: bool,
     *     activity_streams_email_notifications: bool,
     *     state: string,
     *     number_of_edits: int,
     *     fullname: ?string,
     *     number_created_packages: int,
     *     apikey: string,
     *     email: ?string,
     *     num_followers: ?int,
     *     password_hash: ?string
     * } The properties of the created user
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function create(array $data): array
    {
        $response = $this->httpService->post('api/3/action/user_create', new Payload($data));

        if ($response->hasStatus(200) && $response->hasValidJson('user_create.json')) {
            $result = $response->json(true)['result'];

            $this->dispatchEvent(new UserCreated($this->instanceId, $result['id']));

            return $result;
        }

        throw new ResponseException($response);
    }

    /**
     * Request properties of a user and return them. If the request results in a response with HTTP
     * status code 404 create a new user and return its properties.
     *
     * @param string $nameOrId The CKAN ID or name of the user
     *
     * @return array{
     *     email_hash: string,
     *     about: ?string,
     *     display_name: string,
     *     name: string,
     *     created: string,
     *     id: string,
     *     sysadmin: bool,
     *     activity_streams_email_notifications: bool,
     *     state: string,
     *     number_of_edits: int,
     *     fullname: ?string,
     *     number_created_packages: int,
     *     apikey: ?string,
     *     email: ?string,
     *     num_followers: ?int,
     *     password_hash: ?string
     * } The properties of the user
     *
     * @throws ResponseException Thrown when the get user API request failed for any reason except
     *                           for status code 404 or when the create user API request failed for
     *                           any reason
     * @throws ClientException   Thrown when one of the requests could not be sent
     *
     * @see UserRepository::get()
     * @see UserRepository::create()
     */
    public function getOrCreate(string $nameOrId): array
    {
        try {
            return $this->get($nameOrId);
        } catch (ResponseException $e) {
            if ($e->response->hasStatus(404)) {
                return $this->create($this->generateUserData($nameOrId));
            }

            throw $e;
        }
    }

    /**
     * Deletes a user with the given name or id by performing a
     * `api/3/action/user_delete` API call and returning its response.
     *
     * @param string $nameOrId The name or id of the user to be deleted
     *
     * @return bool Whether the deletion was a success
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function delete(string $nameOrId): bool
    {
        $response = $this->httpService->post('api/3/action/user_delete', new Payload(['id' => $nameOrId]));

        if ($response->hasStatus(200) && $response->hasValidJson('user_delete.json')) {
            return true === $response->json(true)['success'];
        }

        throw new ResponseException($response);
    }

    /**
     * Checks whether a given user exists. If the user can be fetched successfully return
     * true, else if the HTTP status code of the response is 404 return false.
     *
     * @param string $nameOrId The CKAN ID or name of the user
     *
     * @return bool Whether the user exists in CKAN
     *
     * @throws ResponseException Thrown when the API request failed for any reason except for status
     *                           code 404
     * @throws ClientException   Thrown when the request could not be sent
     *
     * @see UserRepository::get()
     */
    public function exists(string $nameOrId): bool
    {
        try {
            $this->get($nameOrId);

            return true;
        } catch (ResponseException $e) {
            if ($e->response->hasStatus(404)) {
                return false;
            }

            throw $e;
        }
    }

    /**
     * Generates the user data for a new CKAN user.
     *
     * @param string $name The username for the CKAN user
     *
     * @return array{name: string, email: string, password: string} The generated user data
     */
    private function generateUserData(string $name): array
    {
        $randomizer = new Randomizer();
        $password   = '';

        for ($i = 0; $i < 32; ++$i) {
            $password .= chr($randomizer->getInt(32, 126));
        }

        return [
            'name'     => $name,
            'email'    => $name . '@ckan.xs',
            'password' => $randomizer->shuffleBytes($password),
        ];
    }
}
