<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class CoreRepository.
 *
 * Provides access to some of the more 'basic' API routes offered by CKAN, such as the endpoint to
 * retrieve the status of the CKAN installation.
 */
class CoreRepository
{
    use IdentifiesInstance;

    /**
     * CoreRepository constructor.
     *
     * @param HttpRequestService $httpService The service for interacting with the HTTP API
     * @param null|string        $instanceId  The (optional) ID of the CKAN instance
     */
    public function __construct(protected readonly HttpRequestService $httpService,
                                ?string $instanceId = null)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Request the status of the CKAN installation by performing a `api/3/action/status_show` API
     * call and returning its response.
     *
     * @return array{
     *     ckan_version: string,
     *     site_url: string,
     *     site_description: ?string,
     *     site_title: string,
     *     error_emails_to: ?string,
     *     locale_default: string,
     *     extensions: array<int, string>
     * } The status of the CKAN installation
     *
     * @throws ResponseException Thrown when the API request did not succeed for any reason
     * @throws ClientException   Thrown when the request could not be sent
     */
    public function status(): array
    {
        $response = $this->httpService->get('api/3/action/status_show');

        if ($response->hasStatus(200) && $response->hasValidJson('status_show.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }
}
