<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk\Repository;

use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\IdentifiesInstance;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * Class MembershipRepository.
 *
 * Provides access to organization membership routes offered by CKAN.
 */
class MembershipRepository
{
    use IdentifiesInstance;

    /**
     * MembershipRepository constructor.
     *
     * @param HttpRequestService $httpService The service for interacting with the HTTP API
     * @param null|string        $instanceId  The (optional) ID of the CKAN instance
     */
    public function __construct(protected readonly HttpRequestService $httpService,
                                ?string $instanceId = null)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Make a user member of an organization by performing a
     * `api/3/action/organization_member_create` API call and returning its response.
     *
     * @return array{
     *     capacity: string,
     *     state: string,
     *     table_id: string,
     *     table_name: string,
     *     revision_id: string,
     *     group_id: string,
     *     id: string
     * } The response of the API call
     *
     * @throws ResponseException Thrown when an API request did not succeed for any reason
     * @throws ClientException   Thrown when a request could not be sent
     */
    public function addUser(string $organizationId, string $userNameOrId, string $role): array
    {
        $response = $this->httpService->post(
            'api/3/action/organization_member_create',
            new Payload(['id' => $organizationId, 'username' => $userNameOrId, 'role' => $role])
        );

        if ($response->hasStatus(200) && $response->hasValidJson('organization_member_create.json')) {
            return $response->json(true)['result'];
        }

        throw new ResponseException($response);
    }

    /**
     * Delete a user membership of an organization by performing a
     * `api/3/action/organization_member_delete` API call and returning its response.
     *
     * @return bool Whether the deletion was a success
     *
     * @throws ResponseException Thrown when an API request did not succeed for any reason
     * @throws ClientException   Thrown when a request could not be sent
     */
    public function deleteUser(string $organizationId, string $userNameOrId): bool
    {
        $response = $this->httpService->post(
            'api/3/action/organization_member_delete',
            new Payload(['id' => $organizationId, 'username' => $userNameOrId])
        );

        if ($response->hasStatus(200) && $response->hasValidJson('organization_member_delete.json')) {
            return true === $response->json(true)['success'];
        }

        throw new ResponseException($response);
    }
}
