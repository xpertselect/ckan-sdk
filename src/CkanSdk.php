<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk;

use Psr\EventDispatcher\EventDispatcherInterface;
use XpertSelect\CkanSdk\Repository\CoreRepository;
use XpertSelect\CkanSdk\Repository\DatasetRepository;
use XpertSelect\CkanSdk\Repository\MembershipRepository;
use XpertSelect\CkanSdk\Repository\OrganizationRepository;
use XpertSelect\CkanSdk\Repository\ResourceRepository;
use XpertSelect\CkanSdk\Repository\UserRepository;

/**
 * Class CkanSdk.
 *
 * Sdk for interacting with the HTTP API of a CKAN installation.
 */
class CkanSdk
{
    use IdentifiesInstance;

    /**
     * The repository for interacting with the basic API endpoints of CKAN.
     */
    protected ?CoreRepository $coreRepository = null;

    /**
     * The repository for interacting with the user API endpoints of CKAN.
     */
    protected ?UserRepository $userRepository = null;

    /**
     * The repository for interacting with the organization API endpoints of CKAN.
     */
    protected ?OrganizationRepository $organizationRepository = null;

    /**
     * The repository for interacting with the organization membership API endpoints of CKAN.
     */
    protected ?MembershipRepository $membershipRepository = null;

    /**
     * The repository for interacting with the dataset/package API endpoints of CKAN.
     */
    protected ?DatasetRepository $datasetRepository = null;

    /**
     * The repository for interacting with the resource API endpoints of CKAN.
     */
    protected ?ResourceRepository $resourceRepository = null;

    /**
     * CkanSdk constructor.
     *
     * @param HttpRequestService            $requestService  The service for interacting with the
     *                                                       HTTP api
     * @param null|EventDispatcherInterface $eventDispatcher The service for dispatching events
     * @param null|string                   $instanceId      The (optional) ID of the CKAN instance
     */
    public function __construct(protected HttpRequestService $requestService,
                                protected ?EventDispatcherInterface $eventDispatcher = null,
                                ?string $instanceId = null)
    {
        $this->instanceId = $instanceId;
    }

    /**
     * Retrieve the repository for interacting with the basic API endpoints of CKAN. This repository
     * is instantiated on the first request.
     *
     * Custom implementations of the CoreRepository can be 'injected' by providing a custom
     * implementation of the `makeCoreRepository` method.
     *
     * @return CoreRepository The repository for interacting with the basic API endpoints
     *
     * @see CkanSdk::makeCoreRepository()
     */
    public function core(): CoreRepository
    {
        if (is_null($this->coreRepository)) {
            $this->coreRepository = $this->makeCoreRepository();
        }

        return $this->coreRepository;
    }

    /**
     * Retrieve the repository for interacting with the user API endpoints of CKAN. This repository
     * is instantiated on the first request.
     *
     * Custom implementations of the UserRepository can be 'injected' by providing a custom
     * implementation of the `makeUserRepository` method.
     *
     * @return UserRepository The repository for interacting with the user API endpoints
     *
     * @see CkanSdk::makeUserRepository()
     */
    public function users(): UserRepository
    {
        if (is_null($this->userRepository)) {
            $this->userRepository = $this->makeUserRepository();
        }

        return $this->userRepository;
    }

    /**
     * Retrieve the repository for interacting with the organization API endpoints of CKAN. This
     * repository is instantiated on the first request.
     *
     * Custom implementations of the organizationRepository can be 'injected' by providing a custom
     * implementation of the `makeOrganizationRepository` method.
     *
     * @return OrganizationRepository The repository for interacting with the organization API
     *                                endpoints
     *
     * @see CkanSdk::makeOrganizationRepository()
     */
    public function organizations(): OrganizationRepository
    {
        if (is_null($this->organizationRepository)) {
            $this->organizationRepository = $this->makeOrganizationRepository();
        }

        return $this->organizationRepository;
    }

    /**
     * Retrieve the repository for interacting with the organization membership API endpoints of
     * CKAN. This repository is instantiated on the first request.
     *
     * Custom implementations of the MembershipRepository can be 'injected' by providing a custom
     * implementation of the `makeMembershipRepository` method.
     *
     * @return MembershipRepository The repository for interacting with the organization membership
     *                              API endpoints
     *
     * @see CkanSdk::makeMembershipRepository()
     */
    public function organizationMemberships(): MembershipRepository
    {
        if (is_null($this->membershipRepository)) {
            $this->membershipRepository = $this->makeMembershipRepository();
        }

        return $this->membershipRepository;
    }

    /**
     * Retrieve the repository for interacting with the dataset/package API endpoints of CKAN. This
     * repository is instantiated on the first request.
     *
     * Custom implementations of the datasetRepository can be 'injected' by providing a custom
     * implementation of the `makeDatasetRepository` method.
     *
     * @return DatasetRepository The repository for interacting with the dataset/package API
     *                           endpoints
     *
     * @see CkanSdk::makeDatasetRepository()
     */
    public function datasets(): DatasetRepository
    {
        if (is_null($this->datasetRepository)) {
            $this->datasetRepository = $this->makeDatasetRepository();
        }

        return $this->datasetRepository;
    }

    /**
     * Retrieve the repository for interacting with the resource API endpoints of CKAN. This
     * repository is instantiated on the first request.
     *
     * Custom implementations of the resourceRepository can be 'injected' by providing a custom
     * implementation of the `makeResourceRepository` method.
     *
     * @return ResourceRepository The repository for interacting with the resource API endpoints
     *
     * @see CkanSdk::makeResourceRepository()
     */
    public function resources(): ResourceRepository
    {
        if (is_null($this->resourceRepository)) {
            $this->resourceRepository = $this->makeResourceRepository();
        }

        return $this->resourceRepository;
    }

    /**
     * Creates a CoreRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return CoreRepository The created instance
     */
    protected function makeCoreRepository(): CoreRepository
    {
        return new CoreRepository(
            $this->requestService,
            $this->instanceId
        );
    }

    /**
     * Creates a UserRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return UserRepository The created instance
     */
    protected function makeUserRepository(): UserRepository
    {
        return new UserRepository(
            $this->requestService,
            $this->eventDispatcher,
            $this->instanceId
        );
    }

    /**
     * Creates an OrganizationRepository implementation. Extending classes may opt to provide their
     * own implementation as a way to inject custom functionality.
     *
     * @return OrganizationRepository The created instance
     */
    protected function makeOrganizationRepository(): OrganizationRepository
    {
        return new OrganizationRepository(
            $this->requestService,
            $this->instanceId
        );
    }

    /**
     * Creates an MembershipRepository implementation. Extending classes may opt to provide their
     * own implementation as a way to inject custom functionality.
     *
     * @return MembershipRepository The created instance
     */
    protected function makeMembershipRepository(): MembershipRepository
    {
        return new MembershipRepository(
            $this->requestService,
            $this->instanceId
        );
    }

    /**
     * Creates an DatasetRepository implementation. Extending classes may opt to provide their
     * own implementation as a way to inject custom functionality.
     *
     * @return DatasetRepository The created instance
     */
    protected function makeDatasetRepository(): DatasetRepository
    {
        return new DatasetRepository(
            $this->requestService,
            $this->eventDispatcher,
            $this->instanceId
        );
    }

    /**
     * Creates a ResourceRepository implementation. Extending classes may opt to provide their own
     * implementation as a way to inject custom functionality.
     *
     * @return ResourceRepository The created instance
     */
    protected function makeResourceRepository(): ResourceRepository
    {
        return new ResourceRepository(
            $this->requestService,
            $this->eventDispatcher,
            $this->instanceId
        );
    }
}
