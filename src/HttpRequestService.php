<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk;

use Psr\Http\Message\ResponseInterface;
use XpertSelect\PsrTools\HttpRequestService as HRS;
use XpertSelect\PsrTools\PsrResponse;

/**
 * Class HttpRequestService.
 *
 * A service for interacting with the HTTP API of a CKAN installation using a given PSR compliant
 * implementation.
 */
final class HttpRequestService extends HRS
{
    /**
     * {@inheritdoc}
     */
    public function createUserAgent(): string
    {
        return 'xpertselect/ckan-sdk';
    }

    /**
     * {@inheritdoc}
     */
    public function getPsrResponse(ResponseInterface $response): PsrResponse
    {
        return new CkanResponse($response);
    }
}
