<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace XpertSelect\CkanSdk;

/**
 * Trait IdentifiesInstance.
 *
 * Trait that provides utility methods for reasoning about the ID of a connected instance.
 */
trait IdentifiesInstance
{
    /**
     * The ID of the connected instance.
     */
    protected ?string $instanceId = null;

    /**
     * Determine if an instance ID is present in this instance.
     *
     * @return bool Whether an instance ID is present
     */
    public function hasInstanceId(): bool
    {
        return !is_null($this->getInstanceId());
    }

    /**
     * Retrieve the ID of the instance.
     *
     * @return null|string The ID of the instance
     */
    public function getInstanceId(): ?string
    {
        return $this->instanceId;
    }
}
