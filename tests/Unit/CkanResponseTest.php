<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery as M;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;
use XpertSelect\CkanSdk\CkanResponse;

/**
 * @internal
 */
final class CkanResponseTest extends TestCase
{
    public function testHasValidJson(): void
    {
        $status  = 200;
        $fixture = 'ckan-response/status_show.valid.json';

        $ckanResponse = $this->createMockedResponse($status, $fixture);

        self::assertTrue($ckanResponse->hasStatus($status));
        self::assertTrue($ckanResponse->hasJson());
        self::assertTrue($ckanResponse->hasValidJson('status_show.json'));
        self::assertEquals(json_decode($this->loadFixture($fixture)), $ckanResponse->json());
    }

    public function testHasInvalidJsonAccordingToSchema(): void
    {
        $status  = 200;
        $fixture = 'ckan-response/status_show.invalid.json';

        $ckanResponse = $this->createMockedResponse($status, $fixture);

        self::assertTrue($ckanResponse->hasStatus($status));
        self::assertTrue($ckanResponse->hasJson());
        self::assertFalse($ckanResponse->hasValidJson('status_show.json'));
        self::assertEquals(json_decode($this->loadFixture($fixture)), $ckanResponse->json());
    }

    public function testHasInvalidJson(): void
    {
        $status  = 500;
        $fixture = 'generic_error_response.txt';

        $ckanResponse = $this->createMockedResponse($status, $fixture);

        self::assertTrue($ckanResponse->hasStatus($status));
        self::assertFalse($ckanResponse->hasJson());
        self::assertFalse($ckanResponse->hasValidJson('status_show.json'));
        self::assertEquals(json_decode($this->loadFixture($fixture)), $ckanResponse->json());
    }

    public function testOriginalResponse(): void
    {
        $psrResponse  = M::mock(ResponseInterface::class);
        $ckanResponse = new CkanResponse($psrResponse);

        self::assertSame($ckanResponse->getPsrResponse(), $psrResponse);
    }
}
