<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Mockery as M;
use Tests\TestCase;
use XpertSelect\CkanSdk\CkanSdk;
use XpertSelect\CkanSdk\HttpRequestService;

/**
 * @internal
 */
final class CkanSdkTest extends TestCase
{
    public function testSameCoreRepositoryIsReturnedEveryTime(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertSame($sdk->core(), $sdk->core());
    }
}
