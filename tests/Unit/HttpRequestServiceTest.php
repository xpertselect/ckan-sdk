<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Request;
use Mockery as M;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Tests\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\PsrTools\Exception\ClientException;

/**
 * @internal
 */
final class HttpRequestServiceTest extends TestCase
{
    public static function endpointDataset(): array
    {
        return [
            ['https://example.com', 'https://example.com'],
            ['https://example.com/', 'https://example.com'],
            ['https://example.com:8080', 'https://example.com:8080'],
            ['https://example.com:8080/', 'https://example.com:8080'],
            ['https://example.com/foo', 'https://example.com/foo'],
            ['https://example.com/foo/', 'https://example.com/foo'],
            ['https://example.com:8080/foo', 'https://example.com:8080/foo'],
            ['https://example.com:8080/foo/', 'https://example.com:8080/foo'],
        ];
    }

    public function testHasApiKey(): void
    {
        $httpRequestService = new HttpRequestService(
            '',
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        self::assertFalse($httpRequestService->hasApiKey());

        $httpRequestService->setApiKey('foo');

        self::assertTrue($httpRequestService->hasApiKey());
    }

    /**
     * @dataProvider endpointDataset
     */
    public function testTrailingSlashIsRemovedFromEndpoint(string $endpoint, string $validated): void
    {
        $httpRequestService = new HttpRequestService(
            $endpoint,
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        self::assertEquals($validated, $httpRequestService->getEndpoint());
    }

    public function testGetCreatesPsrGetRequest(): void
    {
        try {
            $httpRequestService = new HttpRequestService(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            self::assertEmpty($this->guzzleHistory);

            $httpRequestService->get('bar', ['lorem' => 'ipsum']);

            self::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                self::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                self::assertEquals('GET', strtoupper($request->getMethod()));
                self::assertEquals('/bar', $request->getUri()->getPath());
                self::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

                self::assertEquals(['application/json'], $request->getHeader('Accept'));
                self::assertEquals(['xpertselect/ckan-sdk'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testApiKeyIsIncludedAsAuthorizationHeader(): void
    {
        try {
            $httpRequestService = new HttpRequestService(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            self::assertEmpty($this->guzzleHistory);
            self::assertFalse($httpRequestService->hasApiKey());

            $httpRequestService->setApiKey('foo');
            $httpRequestService->get('/');

            self::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                self::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                self::assertEquals(['foo'], $request->getHeader('Authorization'));
            }
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }
}
