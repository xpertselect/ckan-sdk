<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Event;

use Tests\TestCase;
use XpertSelect\CkanSdk\Event\DatasetRequested;

/**
 * @internal
 */
final class DatasetRequestedTest extends TestCase
{
    public function testRequestedDatasetIsAvailable(): void
    {
        $event = new DatasetRequested(null, 'foo');

        self::assertEquals('foo', $event->getRequestedDataset());
    }

    public function testHasResolvedDatasetIsFalse(): void
    {
        $event = new DatasetRequested(null, 'foo');

        self::assertFalse($event->hasResolvedDataset());
    }

    public function testGetResolvedDatasetIsNull(): void
    {
        $event = new DatasetRequested(null, 'foo');

        self::assertNull($event->getResolvedDataset());
    }

    public function testResolvedDatasetCanBeRetrieved(): void
    {
        $event = new DatasetRequested(null, 'foo');
        $event->setResolvedDataset(['name' => 'foo']);

        self::assertTrue($event->hasResolvedDataset());
        self::assertNotNull($event->getResolvedDataset());
    }

    public function testResolvedDatasetCanBeRemoved(): void
    {
        $event = new DatasetRequested(null, 'foo');
        $event->setResolvedDataset(['name' => 'foo']);

        self::assertTrue($event->hasResolvedDataset());
        self::assertNotNull($event->getResolvedDataset());

        $event->setResolvedDataset(null);

        self::assertFalse($event->hasResolvedDataset());
        self::assertNull($event->getResolvedDataset());
    }
}
