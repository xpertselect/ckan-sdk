<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Event;

use Tests\TestCase;
use XpertSelect\CkanSdk\Event\UserCreated;

/**
 * @internal
 */
final class UserSavedTest extends TestCase
{
    public function testUserEventsGetUserId(): void
    {
        $userId   = 'foo';
        $event    = new UserCreated(null, $userId);

        self::assertEquals($userId, $event->getUserId());
    }
}
