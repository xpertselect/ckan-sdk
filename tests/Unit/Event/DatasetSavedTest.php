<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit\Event;

use Tests\TestCase;
use XpertSelect\CkanSdk\Event\DatasetCreated;
use XpertSelect\CkanSdk\Event\DatasetPatched;
use XpertSelect\CkanSdk\Event\DatasetUpdated;

/**
 * @internal
 */
final class DatasetSavedTest extends TestCase
{
    public static function eventDataset(): array
    {
        return [
            [DatasetCreated::class],
            [DatasetUpdated::class],
            [DatasetPatched::class],
        ];
    }

    /**
     * @dataProvider eventDataset
     */
    public function testDatasetEventsGetNameOrId(string $event): void
    {
        $nameOrId = 'foo';
        $event    = new $event(null, $nameOrId);

        self::assertEquals($nameOrId, $event->getNameOrId());
    }
}
