<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Tests\TestCase;
use XpertSelect\CkanSdk\IdentifiesInstance;

/**
 * @internal
 */
final class IdentifiesInstanceTest extends TestCase
{
    public static function instanceIdDataset(): array
    {
        return [
            'absent'  => [false, null],
            'present' => [true, 'foo'],
        ];
    }

    /**
     * @dataProvider instanceIdDataset
     */
    public function testInstanceIdBehaviour(bool $hasId, null|string $id): void
    {
        $instance = new class ($id) {
            use IdentifiesInstance;

            public function __construct(?string $id)
            {
                $this->instanceId = $id;
            }
        };

        self::assertEquals($hasId, $instance->hasInstanceId());
        self::assertEquals($id, $instance->getInstanceId());
    }
}
