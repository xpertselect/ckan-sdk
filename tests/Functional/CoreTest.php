<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use Tests\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\CoreRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
final class CoreTest extends TestCase
{
    public function testResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/status_show')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/status_show.valid.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $status     = $repository->status();

            self::assertIsArray($status);
            self::assertArrayHasKey('ckan_version', $status);
            self::assertArrayHasKey('site_url', $status);
            self::assertArrayHasKey('site_description', $status);
            self::assertArrayHasKey('site_title', $status);
            self::assertArrayHasKey('error_emails_to', $status);
            self::assertArrayHasKey('locale_default', $status);
            self::assertArrayHasKey('extensions', $status);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/status_show')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/status_show.invalid.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $repository->status();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/status_show')
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/status_show.valid.json'
                    ));
            });

            $repository = new CoreRepository($requestService);
            $repository->status();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }
}
