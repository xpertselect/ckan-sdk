<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Tests\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\ResourceRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * @internal
 */
final class ResourcesTest extends TestCase
{
    public static function validFixtures(): array
    {
        return [
            ['ckan-response/resource_show.valid.1.json'],
            ['ckan-response/resource_show.valid.2.json'],
        ];
    }

    public static function invalidFixtures(): array
    {
        return [
            ['ckan-response/resource_show.invalid.1.json'],
            ['ckan-response/resource_show.invalid.2.json'],
        ];
    }

    public static function getFromDatasetProvider(): array
    {
        return [
            ['foo', [], null],
            ['foo', ['bar' => 'baz'], null],
            ['foo', ['resources' => ''], null],
            ['foo', ['resources' => []], null],
            ['foo', ['resources' => ['foo' => 'bar']], null],
            ['foo', ['resources' => [['bar' => 'baz']]], null],
            ['foo', ['resources' => [['id' => 'foo']]], ['id' => 'foo']],
            [
                'foo',
                ['resources' => [['id' => 'foo'], ['id' => 'bar'], ['id' => 'baz']]],
                ['id'        => 'foo'],
            ],
        ];
    }

    /**
     * @dataProvider validFixtures
     */
    public function testResponseValidatesAgainstJsonScheme(string $fixture): void
    {
        try {
            $id = 'foo';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/resource_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        $fixture
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $resource   = $repository->get($id);

            $this->assertResourceHasRequiredKeys($resource);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidFixtures
     */
    public function testInvalidResponseDoesNotValidateAgainstJsonScheme(string $fixture): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/resource_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        $fixture
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/resource_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/resource_show.valid.1.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider getFromDatasetProvider
     */
    public function testGetFromDataset(string $id, $dataset, array|null $expected): void
    {
        $repository = new ResourceRepository(M::mock(HttpRequestService::class));
        self::assertEquals($expected, $repository->getFromDataset($id, $dataset));
    }

    public function testCreateResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_create.valid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $resource   = $repository->create($resource);

            $this->assertResourceHasRequiredKeys($resource);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_create.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->create($resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/resource_create.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->create($resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_update.valid.json'
                    ));
            });

            $repository  = new ResourceRepository($requestService);
            $resource    = $repository->update($id, $resource);

            $this->assertResourceHasRequiredKeys($resource);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_update.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->update($id, $resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/resource_update.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->update($id, $resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_patch.valid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $resource   = $repository->patch($id, $resource);

            $this->assertResourceHasRequiredKeys($resource);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_patch.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->patch($id, $resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id       = 'lorem';
            $resource = [
                'package_id' => 'foo-bar-baz',
                'foo'        => 'baz',
                'bar'        => 'bar',
                'baz'        => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/resource_patch.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->patch($id, $resource);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id = 'lorem';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')->andReturn($this->createMockedResponse(
                    200,
                    'ckan-response/resource_show.valid.1.json'
                ));

                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_delete.valid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            self::assertTrue($repository->delete($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id = 'lorem';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')->andReturn($this->createMockedResponse(
                    200,
                    'ckan-response/resource_show.valid.1.json'
                ));

                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/resource_delete.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id = 'lorem';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')->andReturn($this->createMockedResponse(
                    200,
                    'ckan-response/resource_show.valid.1.json'
                ));

                $mock->shouldReceive('post')
                    ->with('api/3/action/resource_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/resource_delete.invalid.json'
                    ));
            });

            $repository = new ResourceRepository($requestService);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * Assert that the given resource has the required fields.
     *
     * @param array $resource The resource to test
     */
    private function assertResourceHasRequiredKeys(array $resource): void
    {
        self::assertIsArray($resource);
        self::assertArrayHasKey('created', $resource);
        self::assertArrayHasKey('description', $resource);
        self::assertArrayHasKey('format', $resource);
        self::assertArrayHasKey('id', $resource);
        self::assertArrayHasKey('last_modified', $resource);
        self::assertArrayHasKey('mimetype', $resource);
        self::assertArrayHasKey('name', $resource);
        self::assertArrayHasKey('package_id', $resource);
        self::assertArrayHasKey('position', $resource);
        self::assertArrayHasKey('state', $resource);
        self::assertArrayHasKey('url', $resource);
    }
}
