<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use Tests\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\MembershipRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * @internal
 */
final class MembershipsTest extends TestCase
{
    public function testOrganizationMemberCreateResponseValidatesAgainstJsonSchema(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';
            $role           = 'baz';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_member_create.valid.json'
                    ));
            });

            $repository    = new MembershipRepository($requestService);
            $response      = $repository->addUser($organizationId, $userNameOrId, $role);

            self::assertIsArray($response);
            self::assertArrayHasKey('capacity', $response);
            self::assertArrayHasKey('state', $response);
            self::assertArrayHasKey('table_id', $response);
            self::assertArrayHasKey('table_name', $response);
            self::assertArrayHasKey('revision_id', $response);
            self::assertArrayHasKey('group_id', $response);
            self::assertArrayHasKey('id', $response);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testOrganizationMemberCreateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';
            $role           = 'baz';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_member_create.invalid.json'
                    ));
            });

            $repository = new MembershipRepository($requestService);
            $repository->addUser($organizationId, $userNameOrId, $role);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testOrganizationMemberCreateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';
            $role           = 'baz';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/organization_member_create.valid.json'
                    ));
            });

            $repository = new MembershipRepository($requestService);
            $repository->addUser($organizationId, $userNameOrId, $role);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testOrganizationMemberDeleteResponseValidatesAgainstJsonSchema(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_member_delete.valid.json'
                    ));
            });

            $repository    = new MembershipRepository($requestService);

            self::assertTrue($repository->deleteUser($organizationId, $userNameOrId));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testOrganizationMemberDeleteInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_member_delete.invalid.json'
                    ));
            });

            $repository = new MembershipRepository($requestService);
            $repository->deleteUser($organizationId, $userNameOrId);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testOrganizationMemberDeleteStatusFailsOnNon200StatusCode(): void
    {
        try {
            $organizationId = 'foo';
            $userNameOrId   = 'bar';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/organization_member_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/organization_member_delete.valid.json'
                    ));
            });

            $repository = new MembershipRepository($requestService);
            $repository->deleteUser($organizationId, $userNameOrId);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }
}
