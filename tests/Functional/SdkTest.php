<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Tests\TestCase;
use XpertSelect\CkanSdk\CkanSdk;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\CoreRepository;
use XpertSelect\CkanSdk\Repository\DatasetRepository;
use XpertSelect\CkanSdk\Repository\MembershipRepository;
use XpertSelect\CkanSdk\Repository\OrganizationRepository;
use XpertSelect\CkanSdk\Repository\ResourceRepository;
use XpertSelect\CkanSdk\Repository\UserRepository;

/**
 * @internal
 */
final class SdkTest extends TestCase
{
    public function testDefaultCoreImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(CoreRepository::class, $sdk->core());
    }

    public function testCustomCoreRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->coreRepository = null;
            }

            protected function makeCoreRepository(): CoreRepository
            {
                $this->wasCalled = true;

                return new class () extends CoreRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $core = $sdk->core();

        self::assertInstanceOf(CoreRepository::class, $core);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($core->foo));
    }

    public function testDefaultUserImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(UserRepository::class, $sdk->users());
    }

    public function testCustomUserRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->userRepository = null;
            }

            protected function makeUserRepository(): UserRepository
            {
                $this->wasCalled = true;

                return new class () extends UserRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $users = $sdk->users();

        self::assertInstanceOf(UserRepository::class, $users);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($users->foo));
    }

    public function testDefaultOrganizationImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(OrganizationRepository::class, $sdk->organizations());
    }

    public function testCustomOrganizationRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->organizationRepository = null;
            }

            protected function makeOrganizationRepository(): OrganizationRepository
            {
                $this->wasCalled = true;

                return new class () extends OrganizationRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $organizations = $sdk->organizations();

        self::assertInstanceOf(OrganizationRepository::class, $organizations);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($organizations->foo));
    }

    public function testDefaultMembershipImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(MembershipRepository::class, $sdk->organizationMemberships());
    }

    public function testCustomMembershipRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->membershipRepository = null;
            }

            protected function makeMembershipRepository(): MembershipRepository
            {
                $this->wasCalled = true;

                return new class () extends MembershipRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $organizationMemberships = $sdk->organizationMemberships();

        self::assertInstanceOf(MembershipRepository::class, $organizationMemberships);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($organizationMemberships->foo));
    }

    public function testDefaultDatasetImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(DatasetRepository::class, $sdk->datasets());
    }

    public function testCustomDatasetRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->datasetRepository = null;
            }

            protected function makeDatasetRepository(): DatasetRepository
            {
                $this->wasCalled = true;

                return new class () extends DatasetRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $datasets = $sdk->datasets();

        self::assertInstanceOf(DatasetRepository::class, $datasets);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($datasets->foo));
    }

    public function testDefaultResourceImplementation(): void
    {
        $requestService = M::mock(HttpRequestService::class);
        $sdk            = new CkanSdk($requestService);

        self::assertInstanceOf(ResourceRepository::class, $sdk->resources());
    }

    public function testCustomResourceRepository(): void
    {
        $sdk  = new class () extends CkanSdk {
            public bool $wasCalled = false;

            public function __construct()
            {
                $this->resourceRepository = null;
            }

            protected function makeResourceRepository(): ResourceRepository
            {
                $this->wasCalled = true;

                return new class () extends ResourceRepository {
                    public string $foo = 'bar';

                    public function __construct()
                    {
                    }
                };
            }
        };

        $resources = $sdk->resources();

        self::assertInstanceOf(ResourceRepository::class, $resources);
        self::assertTrue($sdk->wasCalled);
        self::assertTrue(isset($resources->foo));
    }

    public function testInstanceIdPropagatesToRepositories(): void
    {
        $id  = 'foo';
        $sdk = new CkanSdk(
            M::mock(HttpRequestService::class),
            instanceId: $id
        );

        self::assertEquals($id, $sdk->getInstanceId());
        self::assertEquals($id, $sdk->core()->getInstanceId());
        self::assertEquals($id, $sdk->datasets()->getInstanceId());
        self::assertEquals($id, $sdk->organizationMemberships()->getInstanceId());
        self::assertEquals($id, $sdk->organizations()->getInstanceId());
        self::assertEquals($id, $sdk->resources()->getInstanceId());
        self::assertEquals($id, $sdk->users()->getInstanceId());
    }
}
