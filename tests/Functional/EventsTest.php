<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Psr\EventDispatcher\StoppableEventInterface;
use Tests\TestCase;
use XpertSelect\CkanSdk\Event\DatasetCreated;
use XpertSelect\CkanSdk\Event\DatasetPatched;
use XpertSelect\CkanSdk\Event\DatasetSaved;
use XpertSelect\CkanSdk\Event\DatasetUpdated;

/**
 * @internal
 */
final class EventsTest extends TestCase
{
    public static function datasetMutationEventDataset(): array
    {
        return [
            [new DatasetCreated(null, 'foo')],
            [new DatasetUpdated(null, 'foo')],
            [new DatasetPatched(null, 'foo')],
        ];
    }

    /**
     * @dataProvider datasetMutationEventDataset
     */
    public function testDatasetMutationEventsAreStoppable(object $event): void
    {
        self::assertInstanceOf(StoppableEventInterface::class, $event);
    }

    /**
     * @dataProvider datasetMutationEventDataset
     */
    public function testDatasetMutationEventsExtendDatasetSavedClass(object $event): void
    {
        self::assertInstanceOf(DatasetSaved::class, $event);
    }
}
