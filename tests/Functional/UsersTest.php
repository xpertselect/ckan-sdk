<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use GuzzleHttp\Psr7\Response;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\Assert;
use Psr\EventDispatcher\EventDispatcherInterface;
use Tests\TestCase;
use XpertSelect\CkanSdk\CkanResponse;
use XpertSelect\CkanSdk\Event\UserCreated;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\UserRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;

/**
 * @internal
 */
final class UsersTest extends TestCase
{
    public function testResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id = 'foo';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_show.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $user       = $repository->get($id);

            $this->assertUserHasRequiredKeys($user);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_show.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/user_show.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $user = [
                'foo' => 'baz',
                'bar' => 'bar',
                'baz' => 'foo',
            ];

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_create.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $user       = $repository->create($user);

            $this->assertUserHasRequiredKeys($user);
            self::assertArrayHasKey('apikey', $user);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $user = [
                'foo' => 'baz',
                'bar' => 'bar',
                'baz' => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_create.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->create($user);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $user = [
                'foo' => 'baz',
                'bar' => 'bar',
                'baz' => 'foo',
            ];

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/user_create.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->create($user);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id = 'lorem';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_delete.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            self::assertTrue($repository->delete($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id = 'lorem';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_delete.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id = 'lorem';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/user_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/user_delete.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetOrCreateExistingUser(): void
    {
        try {
            $id   = 'foo';
            $user = ['foo' => 'bar'];

            $repository = M::mock(UserRepository::class)->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn($user);

            self::assertEquals(
                $user,
                $repository->getOrCreate($id)
            );
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetOrCreateNonExistingUser(): void
    {
        try {
            $id   = 'foo';
            $user = ['foo' => 'bar'];

            $repository = M::mock(UserRepository::class)->makePartial();
            $repository->shouldReceive('get')->with($id)->andThrows(
                new ResponseException(new CkanResponse(new Response(404)))
            );
            $repository->shouldReceive('create')->andReturn($user);

            self::assertEquals(
                $user,
                $repository->getOrCreate($id)
            );
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetOrCreateFailedGet(): void
    {
        try {
            $id = 'foo';

            $this->expectException(ResponseException::class);

            $repository = M::mock(UserRepository::class)->makePartial();
            $repository->shouldReceive('get')->with($id)->andThrows(
                new ResponseException(new CkanResponse(new Response(500)))
            );

            $repository->getOrCreate($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetOrCreateFailedCreate(): void
    {
        try {
            $id = 'foo';

            $this->expectException(ResponseException::class);

            $repository = M::mock(UserRepository::class)->makePartial();
            $repository->shouldReceive('get')->with($id)->andThrows(
                new ResponseException(new CkanResponse(new Response(404)))
            );
            $repository->shouldReceive('create')->andThrows(
                new ResponseException(new CkanResponse(new Response(500)))
            );

            $repository->getOrCreate($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchCreateEventOnUserCreate(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $response = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['result' => ['id' => 'my-lorem-id']]);
                });

                $mock->shouldReceive('post')
                    ->andReturn($response);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        $eventWasDispatched = true;

                        self::assertInstanceOf(UserCreated::class, $event);
                        self::assertEquals('my-lorem-id', $event->getUserId());
                    });
            });

            $repository = new UserRepository($requestService, $eventDispatcher);
            $repository->create(['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo']);

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_show.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            self::assertTrue($repository->exists($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_show.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->exists($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsFalseOnExpectedStatusCodes(): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        404, 'ckan-response/user_show.not_found.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            self::assertFalse($repository->exists($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListResponseAllFieldsValidatesAgainstJsonSchema(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_list', [
                        'all_fields' => true,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_list.all_fields.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $users      = $repository->list();

            self::assertIsArray($users);
            foreach ($users as $user) {
                $this->assertUserHasRequiredKeys($user);
            }
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListResponseValidatesAgainstJsonSchema(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_list', [
                        'all_fields' => false,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_list.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $users      = $repository->list(false);

            self::assertIsArray($users);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListAllFieldsInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_list', [
                        'all_fields' => true,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_list.all_fields.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_list', [
                        'all_fields' => false,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/user_list.invalid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->list(false);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/user_list', [
                        'all_fields' => true,
                    ])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/user_list.valid.json'
                    ));
            });

            $repository = new UserRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * Assert that the given user has the required fields.
     *
     * @param array<string, mixed> $user The user to test
     */
    private function assertUserHasRequiredKeys(array $user): void
    {
        self::assertIsArray($user);
        self::assertArrayHasKey('email_hash', $user);
        self::assertArrayHasKey('about', $user);
        self::assertArrayHasKey('display_name', $user);
        self::assertArrayHasKey('name', $user);
        self::assertArrayHasKey('created', $user);
        self::assertArrayHasKey('id', $user);
        self::assertArrayHasKey('sysadmin', $user);
        self::assertArrayHasKey('activity_streams_email_notifications', $user);
        self::assertArrayHasKey('state', $user);
        self::assertArrayHasKey('number_of_edits', $user);
        self::assertArrayHasKey('fullname', $user);
        self::assertArrayHasKey('number_created_packages', $user);
    }
}
