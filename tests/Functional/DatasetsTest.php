<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use Psr\EventDispatcher\EventDispatcherInterface;
use Tests\TestCase;
use XpertSelect\CkanSdk\CkanResponse;
use XpertSelect\CkanSdk\Event\DatasetCreated;
use XpertSelect\CkanSdk\Event\DatasetDeleted;
use XpertSelect\CkanSdk\Event\DatasetPatched;
use XpertSelect\CkanSdk\Event\DatasetReceived;
use XpertSelect\CkanSdk\Event\DatasetRequested;
use XpertSelect\CkanSdk\Event\DatasetUpdated;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\DatasetRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;
use XpertSelect\PsrTools\Payload;
use XpertSelect\PsrTools\PsrResponse;

/**
 * @internal
 */
final class DatasetsTest extends TestCase
{
    public static function validGetFixtures(): array
    {
        return [
            ['ckan-response/package_show.valid.1.json'],
            ['ckan-response/package_show.valid.2.json'],
        ];
    }

    public static function invalidGetFixtures(): array
    {
        return [
            ['ckan-response/package_show.invalid.1.json'],
            ['ckan-response/package_show.invalid.2.json'],
        ];
    }

    public static function expectedDoesNotExistResponses(): array
    {
        return [
            [403, 'ckan-response/package_show.forbidden.valid.json'],
            [404, 'ckan-response/package_show.not_found.valid.json'],
        ];
    }

    public static function generatePackageNameSlugifyDataset(): array
    {
        return [
            ['foo', 'bar', 'bar-foo'],
            ['Foo', 'bar', 'bar-foo'],
            ['foo', 'Bar', 'bar-foo'],
            ['fôo', 'bar', 'bar-f-o'],
            ['foo', 'bâr', 'b-r-foo'],
            ['foo', 'bâr', 'b-r-foo'],
            ['foo', '', 'foo'],
            ['f00', 'bar', 'bar-f00'],
            ['foo', 'b2r', 'b2r-foo'],
            [str_repeat('a', 91), '', str_repeat('a', 90)],
        ];
    }

    public static function generatePackageNameWhenAlreadyExistsDataset(): array
    {
        return [
            [0],
            [1],
            [2],
            [3],
        ];
    }

    /**
     * @dataProvider validGetFixtures
     */
    public function testGetResponseValidatesAgainstJsonScheme(string $fixture): void
    {
        try {
            $id             = 'foo';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DatasetRepository($requestService);
            $dataset    = $repository->get($id);

            $this->assertDatasetHasRequiredKeys($dataset);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetUsesEventResultWhenAvailable(): void
    {
        $eventDispatcher = M::mock(EventDispatcherInterface::class, function(MI $mock) {
            $event = new DatasetRequested(null, 'foo');
            $event->setResolvedDataset(['name' => 'foo']);

            $mock->shouldReceive('dispatch')
                ->andReturn($event);
        });

        $repository = new DatasetRepository(
            M::mock(HttpRequestService::class),
            $eventDispatcher
        );

        try {
            $dataset = $repository->get('foo');

            self::assertIsArray($dataset);
            self::assertEquals('foo', $dataset['name']);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testWhenDatasetIsNotResolvedItIsRequestedFromCkan(): void
    {
        $repository = new DatasetRepository(
            M::mock(HttpRequestService::class, function(MI $mock) {
                $getResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn([
                        'result' => [
                            'id'         => 'my-lorem-id',
                            'identifier' => 'https://my-lorem.id',
                        ],
                    ]);
                });

                $mock->shouldReceive('get')->andReturn($getResponse);
            }),
            M::mock(EventDispatcherInterface::class, function(MI $mock) {
                $mock->shouldReceive('dispatch')->andReturnUsing(fn (object $event) => $event);
            })
        );

        try {
            self::assertEquals([
                'id'         => 'my-lorem-id',
                'identifier' => 'https://my-lorem.id',
            ], $repository->get('foo'));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDatasetReceivedEventIsDispatchedDuringGet(): void
    {
        $eventWasDispatched = false;
        $repository         = new DatasetRepository(
            M::mock(HttpRequestService::class, function(MI $mock) {
                $getResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn([
                        'result' => [
                            'id'         => 'my-lorem-id',
                            'identifier' => 'https://my-lorem.id',
                        ],
                    ]);
                });

                $mock->shouldReceive('get')->andReturn($getResponse);
            }),
            M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        if (!$event instanceof DatasetReceived) {
                            return;
                        }

                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetReceived::class, $event);
                        self::assertEquals([
                            'id'         => 'my-lorem-id',
                            'identifier' => 'https://my-lorem.id',
                        ], $event->dataset);
                    });
            })
        );

        try {
            $repository->get('foo');

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider invalidGetFixtures
     */
    public function testGetInvalidResponseDoesNotValidateAgainstJsonScheme(string $fixture): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'foo';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(200, $fixture));
            });

            $repository = new DatasetRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'foo';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/package_show.valid.1.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_list')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_list.valid.json'
                    ));
            });

            $repository  = new DatasetRepository($requestService);
            $datasetList = $repository->list();

            self::assertIsArray($datasetList);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_list')
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_list.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_list')
                    ->andReturn($this->createMockedResponse(
                        500,
                        'ckan-response/package_list.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testSearchResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $query          = ['q' => '*:*'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($query) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_search', $query)
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_search.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $result     = $repository->search($query);

            self::assertIsArray($result);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testSearchInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $query          = ['q' => '*:*'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($query) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_search', $query)
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_search.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->search($query);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testSearchStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $query          = ['q' => '*:*'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($query) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_search', $query)
                    ->andReturn($this->createMockedResponse(
                        500,
                        'ckan-response/package_search.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->search($query);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($dataset) {
                $payload = new Payload();
                $payload->addValues($dataset);
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_create', Payload::class)
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_create.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $dataset    = $repository->create($dataset);

            $this->assertDatasetHasRequiredKeys($dataset);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_create.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->create($dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCreateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_create', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/package_create.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->create($dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_update.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $dataset    = $repository->update($id, $dataset);

            $this->assertDatasetHasRequiredKeys($dataset);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_update.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->update($id, $dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpdateStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_update', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/package_update.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->update($id, $dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpsertUsesCreateWhenDatasetDoesNotExist(): void
    {
        try {
            $createWasCalled   = false;
            $datasetRepository = M::mock(DatasetRepository::class, function(MI $mock) use (&$createWasCalled) {
                $mock->shouldNotReceive('update');
                $mock->shouldReceive('exists')->with('foo')->andReturnFalse();
                $mock->shouldReceive('create')
                    ->andReturnUsing(function() use (&$createWasCalled) {
                        $createWasCalled = true;

                        return [];
                    });
            })->makePartial();

            $datasetRepository->upsert(['name' => 'foo']);

            self::assertTrue($createWasCalled);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testUpsertUsesUpdateWhenDatasetDoesExist(): void
    {
        try {
            $updateWasCalled   = false;
            $datasetRepository = M::mock(DatasetRepository::class, function(MI $mock) use (&$updateWasCalled) {
                $mock->shouldNotReceive('create');
                $mock->shouldReceive('exists')->with('foo')->andReturnTrue();
                $mock->shouldReceive('update')
                    ->andReturnUsing(function() use (&$updateWasCalled) {
                        $updateWasCalled = true;

                        return [];
                    });
            })->makePartial();

            $datasetRepository->upsert(['name' => 'foo']);

            self::assertTrue($updateWasCalled);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_patch.valid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $dataset    = $repository->patch($id, $dataset);

            $this->assertDatasetHasRequiredKeys($dataset);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_patch.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->patch($id, $dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPatchStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $dataset        = ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo'];
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_patch', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/package_patch.invalid.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->patch($id, $dataset);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_delete.valid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);

            self::assertTrue($repository->delete($id, true));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_delete.invalid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);
            $repository->delete($id, true);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDeleteStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/package_delete', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/package_delete.invalid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);
            $repository->delete($id, true);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPurgeResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/dataset_purge', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/dataset_purge.valid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);

            self::assertTrue($repository->delete($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPurgeInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/dataset_purge', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/dataset_purge.invalid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testPurgeStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('post')
                    ->with('api/3/action/dataset_purge', M::type(Payload::class))
                    ->andReturn($this->createMockedResponse(
                        409,
                        'ckan-response/dataset_purge.invalid.json'
                    ));
            });

            $repository = M::mock(DatasetRepository::class, [$requestService])->makePartial();
            $repository->shouldReceive('get')->with($id)->andReturn(['id' => $id]);
            $repository->delete($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_show.valid.1.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            self::assertTrue($repository->exists($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/package_show.invalid.1.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->exists($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider expectedDoesNotExistResponses
     */
    public function testExistsFalseOnExpectedStatusCodes(int $status, string $fixture): void
    {
        try {
            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id, $status, $fixture) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        $status,
                        $fixture
                    ));
            });

            $repository = new DatasetRepository($requestService);
            self::assertFalse($repository->exists($id));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testExistsStatusFailsOnNotExpectedStatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $id             = 'lorem';
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/package_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        500,
                        'ckan-response/package_show.invalid.1.json'
                    ));
            });

            $repository = new DatasetRepository($requestService);
            $repository->exists($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider generatePackageNameSlugifyDataset
     */
    public function testGeneratePackageNameSlugify(string $title, string $prefix, string $expected): void
    {
        try {
            $repository = M::mock(DatasetRepository::class)->makePartial();
            $repository->shouldReceive('exists')->andReturnFalse();

            self::assertEquals($expected, $repository->generatePackageName($title, $prefix));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider generatePackageNameWhenAlreadyExistsDataset
     */
    public function testGeneratePackageNameWhenAlreadyExists(int $numberOfExistIsTrue): void
    {
        try {
            $expected = 'foo';
            $exists   = array_fill(0, $numberOfExistIsTrue, true);
            $exists   = array_merge($exists, [false]);

            $repository = M::mock(DatasetRepository::class)->makePartial();
            $repository->shouldReceive('exists')->andReturn(...$exists);

            if ($numberOfExistIsTrue > 0) {
                $expected .= '-' . $numberOfExistIsTrue;
            }

            self::assertEquals($expected, $repository->generatePackageName('foo'));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchCreateEventOnDatasetCreate(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $response = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['result' => ['id' => 'my-lorem-id']]);
                });

                $mock->shouldReceive('post')
                    ->andReturn($response);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetCreated::class, $event);
                        self::assertEquals('my-lorem-id', $event->getNameOrId());
                    });
            });

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->create(['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo']);

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDoesNotDispatchCreateEventOnUnsuccessfulDatasetCreate(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $response = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnFalse();
                });

                $mock->shouldReceive('post')
                    ->andReturn($response);
            });

            $eventDispatcher = new class () implements EventDispatcherInterface {
                public bool $hasDispatched = false;

                public function dispatch(object $event): void
                {
                    $this->hasDispatched = true;
                }
            };

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->create(['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo']);

            self::assertFalse($eventDispatcher->hasDispatched);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchUpdateEventOnDatasetUpdate(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $response = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['result' => ['id' => 'my-lorem-id']]);
                });

                $mock->shouldReceive('post')
                    ->andReturn($response);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetUpdated::class, $event);
                        self::assertEquals('my-lorem-id', $event->getNameOrId());
                    });
            });

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->update('my-lorem-id', ['foo' => 'baz', 'bar' => 'bar', 'baz' => 'foo']);

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchPatchEventOnDatasetPatch(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $response = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['result' => ['id' => 'my-lorem-id']]);
                });

                $mock->shouldReceive('post')
                    ->andReturn($response);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetPatched::class, $event);
                        self::assertEquals('my-lorem-id', $event->getNameOrId());
                    });
            });

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->patch('my-lorem-id', ['bar' => 'bar']);

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchDeleteEventOnDatasetDeleteWithIdentifier(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $getResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn([
                        'result' => [
                            'id'         => 'my-lorem-id',
                            'identifier' => 'https://my-lorem.id',
                        ],
                    ]);
                });
                $deleteResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['success' => true]);
                });

                $mock->shouldReceive('get')
                    ->andReturn($getResponse);
                $mock->shouldReceive('post')
                    ->andReturn($deleteResponse);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        if ($event instanceof DatasetRequested) {
                            return;
                        }

                        if ($event instanceof DatasetReceived) {
                            return;
                        }

                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetDeleted::class, $event);
                        self::assertEquals('my-lorem-id', $event->getNameOrId());
                        self::assertEquals('https://my-lorem.id', $event->getIdentifier());
                    });
            });

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->delete('my-lorem-id');

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testDispatchDeleteEventOnDatasetDeleteWithoutIdentifier(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $getResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn([
                        'result' => [
                            'id'         => 'my-lorem-id',
                        ],
                    ]);
                });
                $deleteResponse = M::mock(CkanResponse::class, function(MI $mock) {
                    $mock->shouldReceive('hasStatus')->andReturnTrue();
                    $mock->shouldReceive('hasValidJson')->andReturnTrue();
                    $mock->shouldReceive('json')->andReturn(['success' => true]);
                });

                $mock->shouldReceive('get')
                    ->andReturn($getResponse);
                $mock->shouldReceive('post')
                    ->andReturn($deleteResponse);
            });

            $eventWasDispatched = false;
            $eventDispatcher    = M::mock(EventDispatcherInterface::class, function(MI $mock) use (&$eventWasDispatched) {
                $mock->shouldReceive('dispatch')
                    ->andReturnUsing(function(object $event) use (&$eventWasDispatched) {
                        if ($event instanceof DatasetRequested) {
                            return;
                        }

                        if ($event instanceof DatasetReceived) {
                            return;
                        }

                        $eventWasDispatched = true;

                        self::assertInstanceOf(DatasetDeleted::class, $event);
                        self::assertEquals('my-lorem-id', $event->getNameOrId());
                        self::assertNull($event->getIdentifier());
                    });
            });

            $repository = new DatasetRepository($requestService, $eventDispatcher);
            $repository->delete('my-lorem-id');

            self::assertTrue($eventWasDispatched);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCountReturnsCountOnValidSearchResponse(): void
    {
        try {
            $count      = 123;
            $query      = ['q' => '*:*'];
            $repository = M::mock(DatasetRepository::class, function(MI $mock) use ($query, $count) {
                $invariantQueryPart = ['rows' => 0];

                $mock->shouldReceive('search')
                    ->with($invariantQueryPart + $query)
                    ->andReturn(['count' => $count]);
            })->makePartial();

            self::assertEquals($count, $repository->count($query));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCountFailsWhenSearchFails(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $query      = ['q' => '*:*'];
            $repository = M::mock(DatasetRepository::class, function(MI $mock) use ($query) {
                $invariantQueryPart = ['rows' => 0];

                $mock->shouldReceive('search')
                    ->with($invariantQueryPart + $query)
                    ->andThrow(new ResponseException(M::mock(PsrResponse::class)));
            })->makePartial();

            $repository->count($query);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testCountInvariantQueryPart(): void
    {
        try {
            $query      = ['q' => '*:*', 'rows' => 123];
            $repository = M::mock(DatasetRepository::class, function(MI $mock) {
                $mock->shouldReceive('search')
                    ->withArgs(function($query): bool {
                        self::assertEquals(0, $query['rows']);

                        return true;
                    })
                    ->andReturn(['count' => 123]);
            })->makePartial();

            $repository->count($query);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testAllReturnsAllDatasetsOnValidSearchResponse(): void
    {
        try {
            $page1      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
            $page2      = [10];
            $query      = ['q' => '*:*'];
            $repository = M::mock(DatasetRepository::class, function(MI $mock) use ($page1, $page2) {
                $mock->shouldReceive('search')
                    ->withArgs(function($query): bool {
                        self::assertEquals(10, $query['rows']);

                        return true;
                    })
                    ->twice()
                    ->andReturn(
                        ['count' => 11, 'results' => $page1],
                        ['count' => 11, 'results' => $page2]
                    );
            })->makePartial();

            self::assertEquals(array_merge($page1, $page2), $repository->all($query));
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * Assert that the given dataset has the required fields.
     *
     * @param array $dataset The dataset to test
     */
    private function assertDatasetHasRequiredKeys(array $dataset): void
    {
        self::assertIsArray($dataset);
        self::assertArrayHasKey('creator_user_id', $dataset);
        self::assertArrayHasKey('id', $dataset);
        self::assertArrayHasKey('isopen', $dataset);
        self::assertArrayHasKey('license_id', $dataset);
        self::assertArrayHasKey('license_title', $dataset);
        self::assertArrayHasKey('license_url', $dataset);
        self::assertArrayHasKey('metadata_created', $dataset);
        self::assertArrayHasKey('metadata_modified', $dataset);
        self::assertArrayHasKey('name', $dataset);
        self::assertArrayHasKey('notes', $dataset);
        self::assertArrayHasKey('num_resources', $dataset);
        self::assertArrayHasKey('num_tags', $dataset);
        self::assertArrayHasKey('organization', $dataset);
        self::assertArrayHasKey('owner_org', $dataset);
        self::assertArrayHasKey('private', $dataset);
        self::assertArrayHasKey('state', $dataset);
        self::assertArrayHasKey('title', $dataset);
        self::assertArrayHasKey('type', $dataset);
        self::assertArrayHasKey('title', $dataset);
        self::assertArrayHasKey('groups', $dataset);
        self::assertArrayHasKey('resources', $dataset);
        self::assertArrayHasKey('tags', $dataset);

        $organization = $dataset['organization'] ?? [];
        self::assertIsArray($organization);
        self::assertArrayHasKey('id', $organization);
        self::assertArrayHasKey('name', $organization);
        self::assertArrayHasKey('title', $organization);
        self::assertArrayHasKey('type', $organization);
        self::assertArrayHasKey('description', $organization);
        self::assertArrayHasKey('image_url', $organization);
        self::assertArrayHasKey('created', $organization);
        self::assertArrayHasKey('is_organization', $organization);
        self::assertArrayHasKey('approval_status', $organization);
        self::assertArrayHasKey('state', $organization);

        foreach ($dataset['resources'] ?? [] as $resource) {
            self::assertIsArray($resource);
            self::assertArrayHasKey('created', $resource);
            self::assertArrayHasKey('description', $resource);
            self::assertArrayHasKey('format', $resource);
            self::assertArrayHasKey('id', $resource);
            self::assertArrayHasKey('last_modified', $resource);
            self::assertArrayHasKey('mimetype', $resource);
            self::assertArrayHasKey('name', $resource);
            self::assertArrayHasKey('package_id', $resource);
            self::assertArrayHasKey('position', $resource);
            self::assertArrayHasKey('state', $resource);
            self::assertArrayHasKey('url', $resource);
        }

        foreach ($dataset['tags'] ?? [] as $tag) {
            self::assertIsArray($tag);
            self::assertArrayHasKey('display_name', $tag);
            self::assertArrayHasKey('id', $tag);
            self::assertArrayHasKey('name', $tag);
            self::assertArrayHasKey('state', $tag);
        }
    }
}
