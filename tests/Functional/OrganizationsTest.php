<?php

declare(strict_types=1);

/**
 * This file is part of the xpertselect/ckan-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Functional;

use Mockery as M;
use Mockery\MockInterface as MI;
use Tests\TestCase;
use XpertSelect\CkanSdk\HttpRequestService;
use XpertSelect\CkanSdk\Repository\OrganizationRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * @internal
 */
final class OrganizationsTest extends TestCase
{
    public static function listNamesDataset(): array
    {
        return [
            [[], 'foo', []],
            [[['id' => 'foo']], 'foo', []],
            [[['foo' => 'bar']], 'foo', []],
            [[['id' => 'foo', 'foo' => 'bar']], 'foo', ['foo' => 'bar']],
            [
                [['id' => 'foo', 'foo' => 'bar'], ['id' => 'lorem', 'foo' => 'ipsum']],
                'foo',
                ['foo' => 'bar', 'lorem' => 'ipsum'],
            ],
        ];
    }

    public function testGetResponseValidatesAgainstJsonScheme(): void
    {
        try {
            $id = 'foo';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_show.valid.json'
                    ));
            });

            $repository   = new OrganizationRepository($requestService);
            $organization = $repository->get($id);

            self::assertIsArray($organization);
            self::assertArrayHasKey('users', $organization);
            self::assertArrayHasKey('display_name', $organization);
            self::assertArrayHasKey('description', $organization);
            self::assertArrayHasKey('image_display_url', $organization);
            self::assertArrayHasKey('package_count', $organization);
            self::assertArrayHasKey('created', $organization);
            self::assertArrayHasKey('name', $organization);
            self::assertArrayHasKey('is_organization', $organization);
            self::assertArrayHasKey('state', $organization);
            self::assertArrayHasKey('extras', $organization);
            self::assertArrayHasKey('image_url', $organization);
            self::assertArrayHasKey('groups', $organization);
            self::assertArrayHasKey('type', $organization);
            self::assertArrayHasKey('title', $organization);
            self::assertArrayHasKey('revision_id', $organization);
            self::assertArrayHasKey('num_followers', $organization);
            self::assertArrayHasKey('id', $organization);
            self::assertArrayHasKey('tags', $organization);
            self::assertArrayHasKey('approval_status', $organization);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_show.invalid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testGetStatusFailsOnNon200StatusCode(): void
    {
        try {
            $id = 'foo';
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($id) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_show', ['id' => $id])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/organization_show.valid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->get($id);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListResponseValidatesAgainstJsonSchema(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list', [
                        'all_fields' => true,
                        'offset'     => 0,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list.valid.json'
                    ));
            });

            $repository    = new OrganizationRepository($requestService);
            $organizations = $repository->list();

            self::assertIsArray($organizations);
            foreach ($organizations as $organization) {
                self::assertArrayHasKey('display_name', $organization);
                self::assertArrayHasKey('description', $organization);
                self::assertArrayHasKey('image_display_url', $organization);
                self::assertArrayHasKey('package_count', $organization);
                self::assertArrayHasKey('created', $organization);
                self::assertArrayHasKey('name', $organization);
                self::assertArrayHasKey('is_organization', $organization);
                self::assertArrayHasKey('state', $organization);
                self::assertArrayHasKey('image_url', $organization);
                self::assertArrayHasKey('type', $organization);
                self::assertArrayHasKey('title', $organization);
                self::assertArrayHasKey('revision_id', $organization);
                self::assertArrayHasKey('num_followers', $organization);
                self::assertArrayHasKey('id', $organization);
                self::assertArrayHasKey('approval_status', $organization);
            }
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list', [
                        'all_fields' => true,
                        'offset'     => 0,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list.invalid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListPaginatedResponse(): void
    {
        try {
            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list', [
                        'all_fields' => true,
                        'offset'     => 0,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list.valid.page.1.json'
                    ));
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list', [
                        'all_fields' => true,
                        'offset'     => 25,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list.valid.json'
                    ));
            });

            $repository    = new OrganizationRepository($requestService);
            $organizations = $repository->list();

            self::assertCount(27, $organizations);
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListStatusFailsOnNon200StatusCode(): void
    {
        try {
            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list', [
                        'all_fields' => true,
                        'offset'     => 0,
                    ])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/organization_list.valid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->list();
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListForUserResponseValidatesAgainstJsonSchema(): void
    {
        try {
            $nameOrId   = 'foo';
            $permission = 'read';

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($nameOrId, $permission) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list_for_user', [
                        'all_fields' => true,
                        'limit'      => 1000,
                        'id'         => $nameOrId,
                        'permission' => $permission,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list_for_user.valid.json'
                    ));
            });

            $repository    = new OrganizationRepository($requestService);
            $organizations = $repository->listForUser($nameOrId, $permission);

            self::assertIsArray($organizations);
            foreach ($organizations as $organization) {
                self::assertArrayHasKey('capacity', $organization);
                self::assertArrayHasKey('display_name', $organization);
                self::assertArrayHasKey('description', $organization);
                self::assertArrayHasKey('image_display_url', $organization);
                self::assertArrayHasKey('created', $organization);
                self::assertArrayHasKey('name', $organization);
                self::assertArrayHasKey('is_organization', $organization);
                self::assertArrayHasKey('state', $organization);
                self::assertArrayHasKey('image_url', $organization);
                self::assertArrayHasKey('type', $organization);
                self::assertArrayHasKey('title', $organization);
                self::assertArrayHasKey('id', $organization);
                self::assertArrayHasKey('approval_status', $organization);
            }
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListForUserInvalidResponseDoesNotValidateAgainstJsonScheme(): void
    {
        try {
            $nameOrId   = 'foo';
            $permission = 'read';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($nameOrId, $permission) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list_for_user', [
                        'all_fields' => true,
                        'limit'      => 1000,
                        'id'         => $nameOrId,
                        'permission' => $permission,
                    ])
                    ->andReturn($this->createMockedResponse(
                        200,
                        'ckan-response/organization_list_for_user.invalid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->listForUser($nameOrId, $permission);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    public function testListForUserStatusFailsOnNon200StatusCode(): void
    {
        try {
            $nameOrId   = 'foo';
            $permission = 'read';

            $this->expectException(ResponseException::class);

            $requestService = M::mock(HttpRequestService::class, function(MI $mock) use ($nameOrId, $permission) {
                $mock->shouldReceive('get')
                    ->with('api/3/action/organization_list_for_user', [
                        'all_fields' => true,
                        'limit'      => 1000,
                        'id'         => $nameOrId,
                        'permission' => $permission,
                    ])
                    ->andReturn($this->createMockedResponse(
                        404,
                        'ckan-response/organization_list_for_user.valid.json'
                    ));
            });

            $repository = new OrganizationRepository($requestService);
            $repository->listForUser($nameOrId, $permission);
        } catch (ClientException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider listNamesDataset
     */
    public function testListNames(array $organizationList, string $labelField,
                                  array $expected): void
    {
        try {
            $repository = M::mock(OrganizationRepository::class)->makePartial();
            $repository->shouldReceive('list')->andReturn($organizationList);

            self::assertEquals(
                $expected,
                $repository->names($labelField)
            );
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }

    /**
     * @dataProvider listNamesDataset
     */
    public function testListNamesForUser(array $organizationList, string $labelField,
                                         array $expected): void
    {
        try {
            $repository = M::mock(OrganizationRepository::class)->makePartial();
            $repository->shouldReceive('listForUser')->andReturn($organizationList);

            self::assertEquals(
                $expected,
                $repository->namesForUser('foo', labelField: $labelField)
            );
        } catch (ClientException|ResponseException $e) {
            self::fail($e->getMessage());
        }
    }
}
