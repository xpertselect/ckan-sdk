# XpertSelect / CKAN SDK

[gitlab.com/xpertselect/ckan-sdk](https://gitlab.com/xpertselect/ckan-sdk)

XpertSelect package providing a SDK for interacting with the API of a [CKAN](https://ckan.org/) installation.

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of [`xpertselect/ckan-sdk`](https://packagist.org/packages/xpertselect/ckan-sdk) is done via [Composer](https://getcomposer.org).

```shell
composer require xpertselect/ckan-sdk
```
