# Changelog

## Unreleased (`dev-main`)

### Added

- The `DatasetRepository` now offers an `upsert` method that allows you to create or update a dataset. This is a 
  convenience method that wraps the `exists`, `create` and `update` methods.

### Changed

### Fixed

---

---

## Release 1.3.0

### Added

- A new function for counting datasets is added to `DatasetRepository`: `DatasetRepository::count()`.  
- A new function for getting all datasets is added to `DatasetRepository`: `DatasetRepository::all()`.  

## Release 1.2.0

### Changed

- The CKAN user creation process now uses the userdata generated from `UserRepository::generateUserData` allowing for 
  projects to override this behaviour by implementing their own version of this method.

---

## Release 1.1.0

### Changed

- Upgraded the `xpertselect/tools` dependency and use its new syntax for `Payload` objects.

---

## Release 1.0.1

### Changed

- Updated composer version requirement of `psr/http-message` to allow both major releases.

---

## Release 1.0.0

### Changed

- Updated to php 8.2

---

## Release 0.6.0

### Changed

- Updated various dependencies

---

## Release 0.5.2

### Changed

- Updated various dependencies.

---

## Release 0.5.1

### Fixed

- When retrieving a dataset from CKAN, allow spaces in the resources url field.

---

## Release 0.5.0

### Added

- It is now possible to assign an ID to a `CkanSdk` instance and all underlying repository instances. This ID is included 
  in all dispatched events. This can be used to identify the source of the event when reasoning about multiple CKAN 
  instances.

---

## Release 0.4.0

### Changed

- The `ResourceRepository` now fires `DatasetUpdated` events after creating, updating, patching or deleting a resource.

---

## Release 0.3.0

### Added

- The `DatasetRepository::get` method now dispatches a `DatasetRequested` event prior to loading a dataset from CKAN. This event enables listeners to resolve the requested dataset such that no communication with CKAN is required.
- The `DatasetRepository::get` method now dispatches a `DatasetReceived` event after loading a dataset from CKAN.

___

## Release 0.2.2

### Fixed

- Updated `xpertselect/psr-tools`, which contained a bug when sending requests with JSON body. 

---

## Release 0.2.1

### Fixed

- Corrected the PHPDoc of the `DatasetRepository::search` method to correctly describe the response object returned.
